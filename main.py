import psycopg2

from typing import List
import logging

connection = psycopg2.connect(
    dbname='company',
    user='KamTim',
    password='password',
    host='localhost',
)


class Salary:
    def add_salaries(salary_ids: List[int], employee_ids: List[int], amounts: List[int]):
        cursor = connection.cursor()
        for (salary_id, employee_id, amount) in zip(salary_ids, employee_ids, amounts):
            if not salary_id:
                connection.rollback()
                return Exception
            elif cursor.execute(f'SELECT id FROM Salary WHERE salary_id={salary_id}'):
                connection.rollback()
                return Exception
            elif cursor.execute(f'SELECT id FROM Salary WHERE employee_id={employee_id}'):
                connection.rollback()
                return Exception
            else:
                try:
                    cursor.execute(f'INSERT INTO "Salary" VALUES ({salary_id}, {employee_id}, {amount})')
                    logging.info(f'Successfully inserted ({salary_id}, {employee_id}, {amount}) into Salary')
                except Exception as e:
                    logging.error(str(e))
                    connection.rollback()
                    return e

        connection.commit()
        cursor.close()


def test_if_no_salary_id():
    Salary.add_salaries([None, None], [1, 2], [1000, 1000])


def test_if_same_salary_id():
    Salary.add_salaries([1, 1], [1, 2], [1000, 1000])


def test_if_same_employee_id():
    Salary.add_salaries([1, 2], [1, 1], [1000, 1000])


test_if_no_salary_id()
test_if_same_employee_id()
test_if_same_employee_id()